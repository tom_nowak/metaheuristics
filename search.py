import numpy as np
import matplotlib.pyplot as plt

def simple_gaussian2d(x, y, x_center, y_center, variance):
    return np.exp(- ((x - x_center)**2  + (y - y_center)**2) / (2 * variance))

def goal_function(x, y):
    return 1.5 * simple_gaussian2d(x, y, 0.2, 0.8, 0.01) + 1.1 * simple_gaussian2d(x, y, 0.3, 0.2, 0.02) + simple_gaussian2d(x, y, 0.8, 0.5, 0.04)

def generate_neighbours(point, neighbours_count, covariance_coefficient=0.0001):
    mean = point
    covariance = np.eye(point.shape[0]) * covariance_coefficient
    return np.random.multivariate_normal(mean, covariance, neighbours_count)

def random_search(point):
    if point.shape[0] != 1:
        raise ValueError(f"random_search expects only one point (shape (1, :)), got shape {point.shape}")
    return generate_neighbours(point[0, :], 1)

def stochastic_climbing(point, generated_points_count=10):
    if point.shape[0] != 1:
        raise ValueError(f"stochastic_climbing expects only one point (shape (1, :)), got shape {point.shape}")
    candidates = generate_neighbours(point[0, :], generated_points_count)
    goal_function_values = goal_function(candidates[:, 0], candidates[:, 1])
    best_point_index = np.argmax(goal_function_values)
    return candidates[best_point_index, :].reshape(1, point.shape[1])

def search(initial_points, search_function, iterations_count):
    points_per_iteration = initial_points.shape[0]
    visited_points = np.empty((iterations_count*points_per_iteration, initial_points.shape[1]))
    points = initial_points
    visited_points[:points_per_iteration, :] = points
    for i in range(1, iterations_count):
        points = search_function(points)
        visited_points[i*points_per_iteration:(i + 1)*points_per_iteration, :] = points
    return visited_points

if __name__ == "__main__":
    initial_points = np.array([0.5, 0.5]).reshape((1, 2))
    visited_points = search(initial_points, stochastic_climbing, 10000)
    min_point = np.amin(visited_points, 0)
    max_point = np.amax(visited_points, 0)
    x, y = np.linspace(min_point[0], max_point[0], 1000), np.linspace(min_point[1], max_point[1], 1000)
    mesh_x, mesh_y = np.meshgrid(x, y)
    z = goal_function(mesh_x, mesh_y)
    plt.contourf(x, y, z, 100)
    plt.plot(visited_points[:, 0], visited_points[:, 1], "ro", markersize=1)
    plt.show()
